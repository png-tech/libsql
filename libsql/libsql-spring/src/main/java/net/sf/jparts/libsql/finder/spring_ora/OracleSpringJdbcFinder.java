package net.sf.jparts.libsql.finder.spring_ora;

import java.math.BigInteger;
import java.sql.Types;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import net.sf.jparts.libsql.common.Configuration;
import net.sf.jparts.libsql.finder.Finder;
import net.sf.jparts.libsql.finder.FinderResult;
import net.sf.jparts.libsql.query.QueryParameter;
import net.sf.jparts.libsql.query.SelectQuery;
import net.sf.jparts.libsql.query.util.ParameterInfo;
import net.sf.jparts.libsql.query.util.TransformationResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import static java.lang.System.lineSeparator;

public class OracleSpringJdbcFinder implements Finder {

    public final static String ROW_MAPPER_HINT = "OracleSpringJdbcFinder.rowMapper";

    public final static String RESULTSET_EXTRACTOR_HINT = "OracleSpringJdbcFinder.resultSetExtractor";

    protected final static String JNDI_NAME_KEY = "jndiName";

    protected String jndiName;

    protected OracleQueryTransformer transformer;

    protected NamedParameterJdbcTemplate template;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    public OracleSpringJdbcFinder() {
        Properties p = Configuration.get(this.getClass());
        initJndiName(p);
        initSpringTemplate();
        initQueryTransformer();
    }

    protected void initJndiName(Properties p) {
        jndiName = Configuration.getPropertyValue(p, JNDI_NAME_KEY, null);
        if (jndiName == null || jndiName.isEmpty()) {
            String msg = JNDI_NAME_KEY + " is not set! Check META-INF/libsql/" +
                    this.getClass().getSimpleName() + ".properties file";
            throw new RuntimeException(msg);
        }
    }

    protected DataSource getDataSource() {
        try {
            Context ctx = new InitialContext();
            try {
                return (DataSource) ctx.lookup(jndiName);
            } finally {
                ctx.close();
            }
        } catch (NamingException ex) {
            String msg = "Cannot find DataSource in JNDI. Object name: \"" + jndiName + "\"";
            throw new RuntimeException(msg, ex);
        } catch (ClassCastException ex) {
            String msg = "Object is not instance of DataSource. Object name: \"" + jndiName + "\"";
            throw new RuntimeException(msg, ex);
        }
    }

    protected void initSpringTemplate() {
        template = new NamedParameterJdbcTemplate(getDataSource());
    }

    protected void initQueryTransformer() {
        transformer = new OracleQueryTransformer();
    }

    @SuppressWarnings({"unchecked"})
    protected <E> void applyQueryParameters(SelectQuery<E> query, Map<String, Object> args) {
        Set<QueryParameter<?>> params = query.getParameters();
        for (QueryParameter<?> p : params) {
            Object value = query.getParameterValue(p);
            if (p.getName() != null) {
                args.put(p.getName(), value);
            } else if (p.getPosition() != null && p.getPosition() >= 0) {
                throw new IllegalArgumentException(getClass().getName() + " does not support positional parameters");
            } else {
                throw new IllegalArgumentException("Invalid parameter in query: name and position are undefined");
            }
        }
    }

    private void applyTransformedParams(Map<ParameterInfo, Object> namedParams, Map<String, Object> args) {
        Set<Map.Entry<ParameterInfo, Object>> ps = namedParams.entrySet();
        for (Map.Entry<ParameterInfo, Object> e : ps) {
            args.put(e.getKey().getParamName(), e.getValue());
        }
    }

    @Override
    public <E> FinderResult<E> find(SelectQuery<E> query) {
        TransformationResult qt = transformer.buildQueryString(query);

        Map<String, Object> args = new HashMap<>();
        applyQueryParameters(query, args);
        applyTransformedParams(qt.getNamedParams(), args);

        String sql = qt.getFullQueryString();
        MapSqlParameterSource params = getParameterSource(args);
        boolean isTotalLength = (query.getHintValue(Finder.NO_TOTAL_LENGTH_HINT) == null);

        if (query.getHintValue(ROW_MAPPER_HINT) != null) {
            return runWithMapper(query, sql, params, isTotalLength);
        } else if (query.getHintValue(RESULTSET_EXTRACTOR_HINT) != null) {
            return runWithExtractor(query, sql, params, isTotalLength);
        } else {
            throw new IllegalArgumentException("SelectQuery does not have " +
                    "neither RowMapper nor ResultSetExtractor instance.");
        }
    }

    @SuppressWarnings("unchecked")
    protected  <E> FinderResult<E> runWithMapper(SelectQuery<E> query, String sql, MapSqlParameterSource params,
            boolean isTotalLength) {

        RowMapper<E> mapper = (RowMapper<E>) query.getHintValue(ROW_MAPPER_HINT);
        if (isTotalLength && (! (mapper instanceof AbstractPagedRowMapper))) {
            mapper = new PagedRowMapperAdapter<>(mapper);
        }
        List<E> data = queryWithLogging(sql, params, mapper);

        int totalLength = (isTotalLength)
                ? ((AbstractPagedRowMapper) mapper).getTotalLength()
                : FinderResult.NO_TOTAL_LENGTH;
        return new FinderResult<>(query.getFirstResult(), totalLength, data);
    }

    @SuppressWarnings("unchecked")
    protected <E> FinderResult<E> runWithExtractor(SelectQuery<E> query, String sql, MapSqlParameterSource params,
            boolean isTotalLength) {

        if (isTotalLength || query.getFirstResult() > 0 || query.getMaxResults() < Integer.MAX_VALUE) {
            throw new IllegalArgumentException("Default implementation does not support paging " +
                    "for ResultSetExtractor. You may implement it by yourself.");
        }

        ResultSetExtractor<E> rse = (ResultSetExtractor<E>) query.getHintValue(RESULTSET_EXTRACTOR_HINT);
        E value = queryWithLogging(sql, params, rse);

        return new FinderResult<>(0, 1, Collections.singletonList(value));
    }

    protected <E> List<E> queryWithLogging(String sql, MapSqlParameterSource params, RowMapper<E> mapper) {
        final long start = System.currentTimeMillis();
        List<E> result = template.query(sql, params, mapper);
        logQueryExecution(start, sql, params);
        return result;
    }

    protected <E> E queryWithLogging(String sql, MapSqlParameterSource params, ResultSetExtractor<E> rse) {
        final long start = System.currentTimeMillis();
        E result = template.query(sql, params, rse);
        logQueryExecution(start, sql, params);
        return result;
    }

    protected void logQueryExecution(long start, String sql, MapSqlParameterSource params) {
        logger.debug("{}", generateLogMessage(start, sql, params));
    }

    protected String generateLogMessage(long start, String sql, MapSqlParameterSource params) {
        final long ms = System.currentTimeMillis() - start;

        StringBuilder sb = new StringBuilder();
        sb.append("(SQL query:").append(lineSeparator()).append(sql).append(lineSeparator());
        sb.append("Query Parameters:").append(lineSeparator());
        for (Map.Entry<String, Object> e : params.getValues().entrySet()) {
            sb.append("    ").append(e.getKey()).append(" -> ")
                    .append(objectToString(e.getValue())).append(lineSeparator());
        }
        sb.append("Executed in ").append(ms).append(" ms)");
        return sb.toString();
    }

    protected String objectToString(Object o) {
        return (o == null) ? "null" : o.toString();
    }

    protected MapSqlParameterSource getParameterSource(Map<String, Object> args) {
        MapSqlParameterSource source = new MapSqlParameterSource(args);
        for (Map.Entry<String, Object> a : args.entrySet()) {
            String param = a.getKey();
            Object value = a.getValue();
            if (value instanceof BigInteger) {
                source.registerSqlType(param, Types.BIGINT);
            } else if (value instanceof java.sql.Date) {
                source.registerSqlType(param, Types.DATE);
            } else if (value instanceof java.sql.Time) {
                source.registerSqlType(param, Types.TIME);
            } else if (value instanceof java.sql.Timestamp) {
                source.registerSqlType(param, Types.TIMESTAMP);
            }
        }
        return source;
    }

}
