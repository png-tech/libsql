package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "notNull")
@XmlAccessorType(XmlAccessType.FIELD)
public class NotNullConstraint implements Constraint {

    private static final long serialVersionUID = -1169261172912318252L;

    @XmlValue
    private String property;

    public NotNullConstraint() {
    }

    public NotNullConstraint(String property) {
        this.property = property;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotNullConstraint that = (NotNullConstraint) o;

        //noinspection RedundantIfStatement
        if (property != null ? !property.equals(that.property) : that.property != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return property != null ? property.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "NotNullConstraint{" + "property='" + property + '\'' + '}';
    }
}
