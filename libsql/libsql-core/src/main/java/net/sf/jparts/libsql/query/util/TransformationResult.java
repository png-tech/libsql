package net.sf.jparts.libsql.query.util;

import java.util.HashMap;
import java.util.Map;

public class TransformationResult {

    private StringBuilder query = new StringBuilder();

    private Map<ParameterInfo, Object> namedParams = new HashMap<>();

    private int paramCounter = 1;

    private String lineSep;

    public TransformationResult() {
        lineSep = "\n";
    }

    public TransformationResult(String fullQueryString) {
        this(fullQueryString, "\n");
    }

    public TransformationResult(String fullQueryString, String lineSep) {
        if (fullQueryString == null || fullQueryString.isEmpty()) {
            throw new IllegalArgumentException("fullQueryString must not be empty or null");
        }
        if (lineSep == null || lineSep.isEmpty()) {
            throw new IllegalArgumentException("lineSeparator must not be empty or null");
        }

        query.append(fullQueryString);
        this.lineSep = lineSep;
    }

    public String getFullQueryString() {
        return query.toString();
    }

    public StringBuilder getQueryBuilder() {
        return query;
    }

    public Map<ParameterInfo, Object> getNamedParams() {
        return namedParams;
    }

    public int getParamCounter() {
        return paramCounter;
    }

    public void setParamCounter(int paramCounter) {
        this.paramCounter = paramCounter;
    }

    public String getLineSeparator() {
        return lineSep;
    }
}
