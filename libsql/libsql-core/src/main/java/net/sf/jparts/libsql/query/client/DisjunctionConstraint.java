package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "disjunction")
@XmlAccessorType(XmlAccessType.FIELD)
public class DisjunctionConstraint implements Constraint {

    private static final long serialVersionUID = -8541082454777255220L;

    @XmlElementWrapper(name = "constraints")
    @XmlElementRefs({
            @XmlElementRef(type = AndConstraint.class)
            ,@XmlElementRef(type = ConjunctionConstraint.class)
            ,@XmlElementRef(type = OrConstraint.class)
            ,@XmlElementRef(type = DisjunctionConstraint.class)
            ,@XmlElementRef(type = SimpleConstraint.class)
            ,@XmlElementRef(type = ContainsConstraint.class)
            ,@XmlElementRef(type = NullConstraint.class)
            ,@XmlElementRef(type = NotNullConstraint.class)
            ,@XmlElementRef(type = NotConstraint.class)
            ,@XmlElementRef(type = InConstraint.class)
            ,@XmlElementRef(type = InIgnoreCaseConstraint.class)
            ,@XmlElementRef(type = BetweenConstraint.class)
            ,@XmlElementRef(type = StartsWithConstraint.class)
            ,@XmlElementRef(type = StartsWithCaseSensitiveConstraint.class)
            ,@XmlElementRef(type = EndsWithConstraint.class)
            ,@XmlElementRef(type = EndsWithCaseSensitiveConstraint.class)
            ,@XmlElementRef(type = EqualsIgnoreCaseConstraint.class)
    })
    private List<Constraint> constraints = new ArrayList<>();

    public DisjunctionConstraint() {
    }

    public DisjunctionConstraint(Constraint... constraints) {
        Collections.addAll(this.constraints, constraints);
    }

    public DisjunctionConstraint(List<Constraint> constraints) {
        this.constraints.addAll(constraints);
    }

    public List<Constraint> getConstraints() {
        return constraints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisjunctionConstraint that = (DisjunctionConstraint) o;

        //noinspection RedundantIfStatement
        if (constraints != null ? !constraints.equals(that.constraints) : that.constraints != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return constraints != null ? constraints.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DisjunctionConstraint{" + "constraints=" + constraints + '}';
    }
}
