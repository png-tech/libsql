package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BooleanTypeHandler implements TypeHandler<Boolean> {

    @Override
    public Boolean getResult(ResultSet rs, String column) throws SQLException {
        boolean r = rs.getBoolean(column);
        return (rs.wasNull()) ? null : r;
    }
}
