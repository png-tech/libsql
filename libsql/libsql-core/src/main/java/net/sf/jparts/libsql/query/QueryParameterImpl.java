package net.sf.jparts.libsql.query;

public class QueryParameterImpl<E> implements QueryParameter<E> {

    private String name;

    private Class<E> clazz;

    private Integer position;

    public QueryParameterImpl(String name, Class<E> clazz) {
        if (name == null || name.trim().length() < 1) {
            throw new IllegalArgumentException("name must not be empty string or null");
        }
        this.name = name.trim();
        this.clazz = clazz;
    }

    public QueryParameterImpl(Integer position, Class<E> clazz) {
        if (position == null || position < 0) {
            throw new IllegalArgumentException("position must not be null or less than 0");
        }
        this.position = position;
        this.clazz = clazz;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class<E> getParameterType() {
        return clazz;
    }

    @Override
    public Integer getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QueryParameterImpl<?> that = (QueryParameterImpl) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        //noinspection RedundantIfStatement
        if (position != null ? !position.equals(that.position) : that.position != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (position != null ? position.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "QueryParameterImpl{" +
                "name='" + ((name != null) ? name : "") + '\'' +
                ", clazz=" + ((clazz != null) ? clazz : "") +
                ", position=" + ((position != null) ? position : "") +
                '}';
    }
}
