package net.sf.jparts.libsql.query.constraints;

/**
 * <code>constraint1 and constraint2 and ... constraintN</code>
 */
public class AndConstraint extends AbstractJoinConstraint {

    protected AndConstraint() {
        joinOperator = "and";
        appendSpaces = true;
    }

}
