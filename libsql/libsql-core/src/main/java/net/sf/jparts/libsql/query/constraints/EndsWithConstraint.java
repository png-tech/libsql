package net.sf.jparts.libsql.query.constraints;

public class EndsWithConstraint implements OnePropertyConstraint {

    private final String propertyName;

    private final Object value;

    protected EndsWithConstraint(String propertyName, Object value) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        if (value == null) {
            throw new IllegalArgumentException("value must not be null");
        }
        this.propertyName = propertyName.trim();
        this.value = value;
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "upper(" + propertyName + ") like upper('%" + value + "')";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EndsWithConstraint that = (EndsWithConstraint) o;

        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = propertyName != null ? propertyName.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
