package net.sf.jparts.libsql.query.util;

// in example
// constraint:  id = 3
// property name: id
// paramName: param_id

public class ParameterInfo {

    private String propertyName;

    private String paramName;

    public ParameterInfo(String paramName) {
        this.paramName = paramName;
    }

    public ParameterInfo(String propertyName, String paramName) {
        this.propertyName = propertyName;
        this.paramName = paramName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParameterInfo that = (ParameterInfo) o;

        if (paramName != null ? !paramName.equals(that.paramName) : that.paramName != null) return false;
        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = propertyName != null ? propertyName.hashCode() : 0;
        result = 31 * result + (paramName != null ? paramName.hashCode() : 0);
        return result;
    }
}
