package net.sf.jparts.libsql.mapping;

public interface ArgumentMapping {

    public Class<?> getType();
}
