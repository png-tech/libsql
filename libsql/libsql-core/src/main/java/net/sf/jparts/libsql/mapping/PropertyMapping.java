package net.sf.jparts.libsql.mapping;

abstract public class PropertyMapping {

    protected String name;

    protected PropertyMapping(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("property name must not be empty or null");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PropertyMapping that = (PropertyMapping) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
