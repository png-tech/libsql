package net.sf.jparts.libsql.mapping;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DiscriminatorMapping {

    private String column;

    private Map<String, ResultMapping> mappings;

    private DiscriminatorMapping() {
        // use Builder
    }

    public String getColumn() {
        return column;
    }

    /**
     * Returns nested mappings for each discriminator value (<code>value -> mapping</code>).
     * @return nested mappins, not <code>null</code>.
     */
    public Map<String, ResultMapping> getMappings() {
        return mappings;
    }

    public static class Builder {

        private DiscriminatorMapping result = new DiscriminatorMapping();

        public Builder(String column) {
            if (column == null || column.isEmpty()) {
                throw new IllegalArgumentException("column must not be empty or null");
            }
            result.column = column;
            result.mappings = new HashMap<>();
        }

        public Builder addCase(String value, String resultId) {
            return this.addCase(value, MappingFactory.getInstance().create(resultId));
        }

        public Builder addCase(String value, ResultMapping mapping) {
            if (value == null || value.isEmpty()) {
                throw new IllegalArgumentException("value must not be empty or null");
            }
            if (mapping == null) {
                throw new IllegalArgumentException("mapping must not be null");
            }
            result.mappings.put(value, mapping);
            return this;
        }

        private void validate() {
            if (result.mappings.isEmpty()) {
                throw new IllegalStateException("discriminator without defined cases");
            }
        }

        public DiscriminatorMapping build() {
            validate();

            result.mappings = Collections.unmodifiableMap(result.mappings);
            return result;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiscriminatorMapping that = (DiscriminatorMapping) o;

        if (column != null ? !column.equals(that.column) : that.column != null) return false;
        //noinspection RedundantIfStatement
        if (mappings != null ? !mappings.equals(that.mappings) : that.mappings != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = column != null ? column.hashCode() : 0;
        result = 31 * result + (mappings != null ? mappings.hashCode() : 0);
        return result;
    }
}
