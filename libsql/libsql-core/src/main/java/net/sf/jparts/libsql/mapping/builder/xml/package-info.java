@XmlSchema(namespace = "http://jparts.sf.net/libsql/mapping", elementFormDefault = QUALIFIED)
package net.sf.jparts.libsql.mapping.builder.xml;

import javax.xml.bind.annotation.XmlSchema;

import static javax.xml.bind.annotation.XmlNsForm.QUALIFIED;
