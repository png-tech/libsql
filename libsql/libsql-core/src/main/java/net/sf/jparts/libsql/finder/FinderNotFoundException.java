package net.sf.jparts.libsql.finder;

public class FinderNotFoundException extends RuntimeException {

    public FinderNotFoundException() {
    }

    public FinderNotFoundException(String message) {
        super(message);
    }

    public FinderNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FinderNotFoundException(Throwable cause) {
        super(cause);
    }
}
