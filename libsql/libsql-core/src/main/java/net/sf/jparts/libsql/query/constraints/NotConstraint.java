package net.sf.jparts.libsql.query.constraints;

public class NotConstraint implements Constraint {

    private final Constraint constraint;

    protected NotConstraint(Constraint constraint) {
        if (constraint == null) {
            throw new IllegalArgumentException("constraint must be non-null");
        }
        this.constraint = constraint;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    @Override
    public String toString() {
        return "not " +  constraint.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotConstraint that = (NotConstraint) o;

        //noinspection RedundantIfStatement
        if (constraint != null ? !constraint.equals(that.constraint) : that.constraint != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return constraint != null ? constraint.hashCode() : 0;
    }
}
