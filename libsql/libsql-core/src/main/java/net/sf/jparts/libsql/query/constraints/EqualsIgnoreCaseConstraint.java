package net.sf.jparts.libsql.query.constraints;

public class EqualsIgnoreCaseConstraint implements OnePropertyConstraint {

    private final String propertyName;

    private final String value;

    protected EqualsIgnoreCaseConstraint(String propertyName, String value) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException("value must not be empty or null");
        }
        this.propertyName = propertyName.trim();
        this.value = value;
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "upper(" + propertyName + ") = upper('" + value + "')";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EqualsIgnoreCaseConstraint that = (EqualsIgnoreCaseConstraint) o;

        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;
        //noinspection RedundantIfStatement
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = propertyName != null ? propertyName.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
