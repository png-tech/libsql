package net.sf.jparts.libsql.query.constraints;

import java.util.Collection;
import java.util.List;

public abstract class Constraints {

    public static Constraint not(Constraint c) {
        return new NotConstraint(c);
    }

    public static Constraint eq(String propertyName, Object value) {
        return new SimpleConstraint(propertyName, "=", value);
    }

    public static Constraint equalsIgnoreCase(String propertyName, Object value) {
        String v = (value == null) ? null : value.toString();
        return new EqualsIgnoreCaseConstraint(propertyName, v);
    }

    public static Constraint equalsIgnoreCase(String propertyName, String value) {
        return new EqualsIgnoreCaseConstraint(propertyName, value);
    }

    public static Constraint ne(String propertyName, Object value) {
        return new SimpleConstraint(propertyName, "<>", value);
    }

    public static Constraint like(String propertyName, Object value) {
        return new SimpleConstraint(propertyName, "like", value);
    }

    public static Constraint gt(String propertyName, Object value) {
        return new SimpleConstraint(propertyName, ">", value);
    }

    public static Constraint lt(String propertyName, Object value) {
        return new SimpleConstraint(propertyName, "<", value);
    }

    public static Constraint ge(String propertyName, Object value) {
        return new SimpleConstraint(propertyName, ">=", value);
    }

    public static Constraint le(String propertyName, Object value) {
        return new SimpleConstraint(propertyName, "<=", value);
    }

    public static Constraint eqProperty(String propertyName, String otherPropertyName) {
        return new PropertyConstraint(propertyName, "=", otherPropertyName);
    }

    public static Constraint neProperty(String propertyName, String otherPropertyName) {
        return new PropertyConstraint(propertyName, "<>", otherPropertyName);
    }

    public static Constraint gtProperty(String propertyName, String otherPropertyName) {
        return new PropertyConstraint(propertyName, ">", otherPropertyName);
    }

    public static Constraint ltProperty(String propertyName, String otherPropertyName) {
        return new PropertyConstraint(propertyName, "<", otherPropertyName);
    }

    public static Constraint geProperty(String propertyName, String otherPropertyName) {
        return new PropertyConstraint(propertyName, ">=", otherPropertyName);
    }

    public static Constraint leProperty(String propertyName, String otherPropertyName) {
        return new PropertyConstraint(propertyName, "<=", otherPropertyName);
    }

    public static Constraint isNull(String propertyName) {
        return new NullConstraint(propertyName);
    }

    public static Constraint isNotNull(String propertyName) {
        return new NotNullConstraint(propertyName);
    }

    public static Constraint between(String propertyName, Object low, Object high) {
        return new BetweenConstraint(propertyName, low, high);
    }

    /**
     * <code>(constraint1 and constraint2 and ... constraintN)</code>
     */
    public static Constraint conjunction(Constraint... constraints) {
        return new ConjunctionConstraint().setConstraints(constraints);
    }

    /**
     * <code>(constraint1 and constraint2 and ... constraintN)</code>
     */
    public static Constraint conjunction(List<Constraint> constraints) {
        return new ConjunctionConstraint().setConstraints(constraints.toArray(new Constraint[constraints.size()]));
    }

    /**
     * <code>constraint1 and constraint2 and ... constraintN</code>
     */
    public static Constraint and(Constraint... constraints) {
        return (new AndConstraint()).setConstraints(constraints);
    }

    /**
     * <code>constraint1 and constraint2 and ... constraintN</code>
     */
    public static Constraint and(List<Constraint> constraints) {
        return (new AndConstraint()).setConstraints(constraints.toArray(new Constraint[constraints.size()]));
    }

    /**
     * <code>(constraint1 or constraint2 or ... constraintN)</code>
     */
    public static Constraint disjunction(Constraint... constraints) {
        return new DisjunctionConstraint().setConstraints(constraints);
    }

    /**
     * <code>(constraint1 or constraint2 or ... constraintN)</code>
     */
    public static Constraint disjunction(List<Constraint> constraints) {
        return new DisjunctionConstraint().setConstraints(constraints.toArray(new Constraint[constraints.size()]));
    }

    /**
     * <code>constraint1 or constraint2 or ... constraintN</code>
     */
    public static Constraint or(Constraint... constraints) {
        return (new OrConstraint()).setConstraints(constraints);
    }

    /**
     * <code>constraint1 or constraint2 or ... constraintN</code>
     */
    public static Constraint or(List<Constraint> constraints) {
        return (new OrConstraint()).setConstraints(constraints.toArray(new Constraint[constraints.size()]));
    }
    
    public static Constraint sql(String sql) {
        return new SqlConstraint(sql);
    }

    public static Constraint simpleConstraint(String propertyName, String operator, Object value) {
        return new SimpleConstraint(propertyName, operator, value);
    }

    public static Constraint propertyConstraint(String propertyName, String operator, String otherPropertyName) {
        return new PropertyConstraint(propertyName, operator, otherPropertyName);
    }

    public static Constraint in(String propertyName, Object... values) {
        return new InConstraint(propertyName, values);
    }

    public static Constraint in(String propertyName, Collection<?> values) {
        return new InConstraint(propertyName, values.toArray());
    }

    public static Constraint inIgnoreCase(String propertyName, Object... values) {
        return new InIgnoreCaseConstraint(propertyName, values);
    }

    public static Constraint inIgnoreCase(String propertyName, Collection<?> values) {
        return new InIgnoreCaseConstraint(propertyName, values.toArray());
    }

    public static Constraint contains(String propertyName, Object value) {
        return new ContainsConstraint(propertyName, value);
    }

    public static Constraint startsWith(String propertyName, Object value) {
        return new StartsWithConstraint(propertyName, value);
    }

    public static Constraint startsWithCs(String propertyName, Object value) {
        return new StartsWithCaseSensitiveConstraint(propertyName, value);
    }

    public static Constraint endsWith(String propertyName, Object value) {
        return new EndsWithConstraint(propertyName, value);
    }

    public static Constraint endsWithCs(String propertyName, Object value) {
        return new EndsWithCaseSensitiveConstraint(propertyName, value);
    }

    private Constraints() {
        // do nothing
    }
}
