package net.sf.jparts.libsql.mapping.builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

public class XmlValidationEventHandler implements ValidationEventHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean handleEvent(ValidationEvent event) {
        if (event.getSeverity() == ValidationEvent.WARNING) {
            logger.warn(event.toString());
            return true;
        }

        // error, fatal_error
        return false;
    }
}
