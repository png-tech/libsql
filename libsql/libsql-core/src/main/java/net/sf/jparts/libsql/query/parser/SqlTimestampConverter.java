package net.sf.jparts.libsql.query.parser;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class SqlTimestampConverter extends AbstractDateConverter<Timestamp> {

    public static final String DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public SqlTimestampConverter() {
        super(DEFAULT_FORMAT);
    }

    public SqlTimestampConverter(final String pattern) {
        super(pattern);
    }

    public SqlTimestampConverter(boolean safe, final String pattern, Timestamp errorValue, Timestamp nullValue) {
        super(safe, pattern, errorValue, nullValue);
    }

    @Override
    protected Timestamp tryParse(String value, SimpleDateFormat format) throws ParseException {
        return new Timestamp(format.parse(value).getTime());
    }
}
