package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "contains")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContainsConstraint implements Constraint {

    private static final long serialVersionUID = 4577521027391189347L;

    private String property;

    private String value;

    public ContainsConstraint() {
    }

    public ContainsConstraint(String property, String value) {
        this.property = property;
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContainsConstraint that = (ContainsConstraint) o;

        if (property != null ? !property.equals(that.property) : that.property != null) return false;
        //noinspection RedundantIfStatement
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = property != null ? property.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ContainsConstraint{" + "property='" + property + '\'' + ", value='" + value + '\'' + '}';
    }
}
