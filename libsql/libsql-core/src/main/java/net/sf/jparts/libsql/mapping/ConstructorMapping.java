package net.sf.jparts.libsql.mapping;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ConstructorMapping {

    private List<ArgumentMapping> arguments;

    public ConstructorMapping(List<ArgumentMapping> arguments) {
        if (arguments == null || arguments.size() < 1) {
            throw new IllegalArgumentException("constructor arguments must not be empty list or null");
        }
        this.arguments = Collections.unmodifiableList(arguments);
    }

    public ConstructorMapping(ArgumentMapping... arguments) {
        this((arguments == null) ? null : Arrays.asList(arguments));
    }

    public List<ArgumentMapping> getArguments() {
        return arguments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConstructorMapping that = (ConstructorMapping) o;

        //noinspection RedundantIfStatement
        if (arguments != null ? !arguments.equals(that.arguments) : that.arguments != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return arguments != null ? arguments.hashCode() : 0;
    }
}
