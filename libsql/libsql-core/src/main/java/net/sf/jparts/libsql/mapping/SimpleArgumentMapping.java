package net.sf.jparts.libsql.mapping;

import net.sf.jparts.libsql.mapping.types.TypeHandler;
import net.sf.jparts.libsql.mapping.types.TypeHandlerRegistry;

public class SimpleArgumentMapping implements ArgumentMapping {

    private String column;

    private Class<?> type;

    private TypeHandler<?> typeHandler;

    private SimpleArgumentMapping() {
        // use Builder, not 'new'
    }

    public String getColumn() {
        return column;
    }

    public Class<?> getType() {
        return type;
    }

    public TypeHandler<?> getTypeHandler() {
        return typeHandler;
    }

    // builder

    public static class Builder {

        private SimpleArgumentMapping result = new SimpleArgumentMapping();

        public Builder(String column) {
            if (column == null || column.isEmpty()) {
                throw new IllegalArgumentException("column must not be null or empty");
            }
            result.column = column;
        }

        public Builder setType(Class<?> type) {
            if (type == null) {
                throw new IllegalArgumentException("type must not be null, column = " + result.column);
            }
            result.type = type;
            return this;
        }

        public Builder setType(String className) {
            if (className == null || className.isEmpty()) {
                throw new IllegalArgumentException("className must not be empty or null, column = " + result.column);
            }
            try {
                result.type = Class.forName(className);
                return this;
            } catch (ClassNotFoundException ex) {
                throw new IllegalArgumentException("column = " + result.column + ", message = " + ex.getMessage(), ex);
            }
        }

        @SuppressWarnings("unchecked")
        public Builder setTypeHandler(String className) {
            if (className == null || className.isEmpty()) {
                throw new IllegalArgumentException("className must not be empty or null, column = " + result.column);
            }

            result.typeHandler = TypeHandlerRegistry.getInstance().getByName(className);
            return this;
        }

        public Builder setTypeHandler(Class<TypeHandler> handlerType) {
            if (handlerType == null) {
                throw new IllegalArgumentException("handlerType must not be null, column = " + result.column);
            }
            result.typeHandler = TypeHandlerRegistry.getInstance().getByName(handlerType.getName());
            return this;
        }

        public Builder setTypeHandler(TypeHandler<?> typeHandler) {
            if (typeHandler == null) {
                throw new IllegalArgumentException("typeHandler must not be null, column = " + result.column);
            }
            result.typeHandler = typeHandler;
            return this;
        }

        private void validate() {
            if (result.type == null) {
                throw new IllegalArgumentException("type is not set for argument mapping, column = " + result.column);
            }
        }

        public SimpleArgumentMapping build() {
            validate();
            return result;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleArgumentMapping that = (SimpleArgumentMapping) o;

        if (column != null ? !column.equals(that.column) : that.column != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        //noinspection RedundantIfStatement
        if (typeHandler != null ? !typeHandler.equals(that.typeHandler) : that.typeHandler != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = column != null ? column.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (typeHandler != null ? typeHandler.hashCode() : 0);
        return result;
    }
}
