package net.sf.jparts.libsql.query.constraints;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for {@link ContainsConstraint}.
 */
public class ContainsConstraintTest {

    private static ContainsConstraint cc;

    @BeforeClass
    public static void setUpBeforeClass() {
        cc = new ContainsConstraint("fullName", "john");
    }

    @AfterClass
    public static void tearDownAfterClass() {
        cc = null;
    }

    @Test
    public void testConstructor() {
        try {
            new ContainsConstraint(null, null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new ContainsConstraint(" ", null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new ContainsConstraint("p", null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new ContainsConstraint("p", " ");
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            fail("Unexpected exception");
        }
    }

    @Test
    public void testGetPropertyName() {
        assertEquals("fullName", cc.getPropertyName());
    }

    @Test
    public void testGetValue() {
        assertEquals("john", cc.getValue());
    }

    @Test
    public void testToString() {
        String ok = "upper(fullName) like upper('%john%')";
        assertEquals(ok, cc.toString());
    }
}
