package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Check dates bugs.
 */
public class DateTimeTypeHandlerTest {
    private DateTimeTypeHandler handler;
    private ResultSet rs;

    @Before
    public void setUp() throws Exception {
        handler = new DateTimeTypeHandler();
        rs = mock(ResultSet.class);
    }

    @Test
    public void testCovert() throws Exception {
        testByDate(2017, 2, 3);
    }

    private void testByDate(int year, int monthOfYear, int dayOfMonth) throws SQLException {
        DateTime checkDate = new DateTime().withDate(year, monthOfYear, dayOfMonth).withTime(0, 0, 0, 0);
        Date dbDate = checkDate.toDate();
        when(rs.getTimestamp("DATE_COLUMN")).thenReturn(new Timestamp(dbDate.getTime()));
        assertEquals(checkDate, handler.getResult(rs, "DATE_COLUMN"));
    }


    /** check bug, handler returns "1899-12-31T23:31:17.000+02:30:17" */
    @Test
    public void testDateConvertBug() throws Exception {

        Date dbDate = new SimpleDateFormat("dd.mm.yyyy").parse("01.01.1900");
        when(rs.getTimestamp("DATE_COLUMN")).thenReturn(new Timestamp(dbDate.getTime()));

        assertEquals(new DateTime().withDate(1900, 1, 1).withTime(0, 1, 0, 0),
                handler.getResult(rs, "DATE_COLUMN"));
    }
}