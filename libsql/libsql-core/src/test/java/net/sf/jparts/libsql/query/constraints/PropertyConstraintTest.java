package net.sf.jparts.libsql.query.constraints;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for {@link PropertyConstraint}.
 */
public class PropertyConstraintTest {

    private static PropertyConstraint pc;

    @BeforeClass
    public static void setUpBeforeClass() {
        pc = new PropertyConstraint("p1", "o", "p2");
    }

    @AfterClass
    public static void tearDownAfterClass() {
        pc = null;
    }

    @Test
    public void testConstructor() {
        try {
            new PropertyConstraint(null, "o", "p2");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new PropertyConstraint(" ", "o", "p2");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new PropertyConstraint("p1", null, "p2");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new PropertyConstraint("p1", "  ", "p2");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new PropertyConstraint("p1", "o", null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new PropertyConstraint("p1", "o", "");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }
    }

    @Test
    public void testGetPropertyName() {
        assertEquals("p1", pc.getPropertyName());
    }

    @Test
    public void testGetOtherPropertyName() {
        assertEquals("p2", pc.getOtherPropertyName());
    }

    @Test
    public void testGetOperator() {
        assertEquals("o", pc.getOperator());
    }

    @Test
    public void testToString() {
        assertEquals("p1 o p2", pc.toString());
    }
}
