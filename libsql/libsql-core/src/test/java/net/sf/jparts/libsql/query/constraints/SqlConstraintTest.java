package net.sf.jparts.libsql.query.constraints;

import java.util.concurrent.ThreadLocalRandom;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for {@link SqlConstraint}.
 */
public class SqlConstraintTest {

    @Test
    public void testConstructor() {
        try {
            new SqlConstraint(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new SqlConstraint("");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new SqlConstraint(" ");
        } catch (Exception ex) {
            ex.printStackTrace();
            fail("Unexpected exception " + ex.getMessage());
        }
    }

    @Test
    public void testToString() {
        assertEquals(" aa bb ", new SqlConstraint(" aa bb ").toString());
    }

    @Test
    public void testEquals() {
        long rnd = ThreadLocalRandom.current().nextLong(1000, Long.MAX_VALUE);
        assertEquals(new SqlConstraint(String.valueOf(rnd)), new SqlConstraint(String.valueOf(rnd)));
    }

}
