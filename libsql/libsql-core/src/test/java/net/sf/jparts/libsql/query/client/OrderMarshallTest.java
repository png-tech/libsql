package net.sf.jparts.libsql.query.client;

import org.junit.Assert;
import net.sf.jparts.libsql.AbstractMarshallTest;
import org.junit.Test;

public class OrderMarshallTest extends AbstractMarshallTest {

    private final String aAsc =
            "<qc:order xmlns:qc=\"http://jparts.sf.net/libsql/query/client\">" +
            "<qc:ascending>true</qc:ascending><qc:propertyName>a</qc:propertyName></qc:order>";

//            "<order>" +
//            "<ascending>true</ascending>" +
//            "<propertyName>a</propertyName>" +
//            "</order>";

    private final String aDesc =
            "<qc:order xmlns:qc=\"http://jparts.sf.net/libsql/query/client\">" +
            "<qc:ascending>false</qc:ascending><qc:propertyName>a</qc:propertyName></qc:order>";

//            "<order>" +
//            "<ascending>false</ascending>" +
//            "<propertyName>a</propertyName>" +
//            "</order>";

    @Test
    public void testMarshallAsc() {
        Order o = new Order("a");
        Assert.assertEquals(aAsc, marshall(o));
    }

    @Test
    public void testMarshallDesc() {
        Order o = new Order("a", false);
        Assert.assertEquals(aDesc, marshall(o));
    }

}
