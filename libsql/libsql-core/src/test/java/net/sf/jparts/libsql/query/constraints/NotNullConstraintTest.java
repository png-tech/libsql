package net.sf.jparts.libsql.query.constraints;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for {@link NotNullConstraint}.
 */
public class NotNullConstraintTest {

    private static NotNullConstraint nnc;

    @BeforeClass
    public static void setUpBeforeClass() {
        nnc = new NotNullConstraint("p");
    }

    @AfterClass
    public static void tearDownAfterClass() {
        nnc = null;
    }

    @Test
    public void testConstructor() {
        try {
            new NotNullConstraint(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new NotNullConstraint("  ");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

    }

    @Test
    public void testGetPropertyName() {
        assertEquals("p", nnc.getPropertyName());
    }

    @Test
    public void testToString() {
        assertEquals("p is not null", nnc.toString());
    }
}
