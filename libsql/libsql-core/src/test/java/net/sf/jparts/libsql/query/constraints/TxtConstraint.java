package net.sf.jparts.libsql.query.constraints;

import org.junit.Ignore;

@Ignore
public class TxtConstraint implements Constraint {

    private static final long serialVersionUID = 4198229689754820966L;

    private final String txt;

    public TxtConstraint(String txt) {
        this.txt = txt;
    }

    @Override
    public String toString() {
        return txt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TxtConstraint that = (TxtConstraint) o;

        if (txt != null ? !txt.equals(that.txt) : that.txt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return txt != null ? txt.hashCode() : 0;
    }
}
