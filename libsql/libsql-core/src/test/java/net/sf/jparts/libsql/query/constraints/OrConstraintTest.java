package net.sf.jparts.libsql.query.constraints;

import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for {@link OrConstraint}.
 */
public class OrConstraintTest {

    @Test
    public void testOr() {
        OrConstraint oc = new OrConstraint();
        oc.setConstraints(new TxtConstraint("t1"), new TxtConstraint("t2"), new TxtConstraint("t3"));

        boolean append = oc.isAppendSpaces();
        assertTrue(append);

        String op = oc.getJoinOperator();
        assertEquals("or", op);

        List<Constraint> constraints = oc.getConstraints();
        assertNotNull(constraints);
        assertEquals(3, constraints.size());

        final String ok = "t1 or t2 or t3";
        String and = oc.toString();
        assertEquals(ok, and);
    }
}
